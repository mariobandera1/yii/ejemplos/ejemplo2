<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionEjercicio1() {

        $alumnos = [
            'mario',
            'Denia',
            'Alalia',
            'ana'
        ];

        return $this->render('ejercicio1',
                        [
                            "alumnos" => $alumnos
        ]);
    }

    public function actionEjercicio2() {
        return $this->render('ejercicio2');
    }

    public function actionEjercicio3() {

        $numero = [1,2,3,4,5,6,7,8];
        
        return $this->render("ejercicio3",
                [
                    "datos"=>$numero
                ]);
         
    }

    public function actionEjercicio4($numero = 3) {

        $imagenes = [
            "111.jfif",
            "222.jfif",
            "333.jfif",
            "444.jfif",
        ];

        $datos = [
            ["nombre" => " pepe ", "apellidos" => " garcia perez ", "edad"=>45],
            ["nombre" => " juan ", "apellidos" => " Sanchez martinex ", "edad"=>24],
            ["nombre" => " Luisa ", "apellidos" => " Gonzalez diaz ", "edad"=>21],
            ["nombre" => " Angela ", "apellidos" => " perez alonso ", "edad"=>32],
        ];

        return $this->render("ejercicio4",
                        [
                            "foto" => $imagenes[$numero],
                            "datos" => $datos[$numero],
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
}
